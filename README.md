# Fractyr #
*A vaguely Descent-inspired abstract 6-degree-of-freedom indoor flight sim / tunnel shooter.*

3D engine and all gameplay written from scratch in five days by a two-man team.

The world is procedurally generated at runtime using 3D Voronoi cells, dynamically triangulated, mapping the interior of a Mandelbulb 3D fractal.

## Info ##
* http://fractyr.com

## Download ##
* http://voxelstorm.itch.io/fractyr

## Bug reporting ##
* http://code.voxelstorm.com/fractyr/issues
